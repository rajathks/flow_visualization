import matplotlib.pyplot as plt
import numpy as np

#def generate_random_matrix(row, col):
#    return np.random.random_integers(4096, size = (row, col))
                
#def plot_matrix(in_matrix):
#    plt.matshow(in_matrix, cmap = plt.cm.gray)
#    plt.show()
#    plt.savefig('test.png')    

#plot_matrix(np.random.random_integers(4096, size = (4, 4)))

try:
    fname = 'results.out'
    matrix_in = np.loadtxt(fname)
except:
    err_msg = "Could not load data from file %s." % fname \
              + " Did you forget to run the program?"
    raise Exception(err_msg)


plt.matshow(matrix_in, cmap = plt.cm.gray)
plt.show()
plt.savefig('test.png')    
