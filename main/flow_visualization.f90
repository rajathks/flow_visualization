program flow_visualization
  !use input_reader_mod
  
  implicit none
  integer :: image_size_row, image_size_col, allocate_status, sub_i
  integer :: midpoint_x, midpoint_y, max_saturation
  real :: radius, scale_factor, total_length, total_width, current_r, difference, current_x, current_y, h, sum
  integer, dimension(:, :), allocatable :: image
  integer :: i, j, current_distance, radial_length, current_i, current_j, fractional_pixel
  integer, parameter :: out_unit=20
  real :: r, theta, x_out, y_out
  logical :: tf
  
  print *, 'Write down the number of pixels (row, cols):'
  read *, image_size_row, image_size_col

  print *, 'Write down the radius of the circle'
  read *, radius
  
  print *, 'Write down the number of meter/pixel'
  read *, scale_factor

  print *, 'Write the max saturation'
  read *, max_saturation
  
  open(unit=out_unit, file = "results.out", action = "write", status = "replace")
  
  total_length = scale_factor / image_size_col
  total_width = scale_factor / image_size_row
  
  midpoint_x = image_size_col / 2
  midpoint_y = image_size_row / 2
  
  allocate(image(image_size_row, image_size_col), STAT = allocate_status)
  if (allocate_status /= 0) stop '*** Not enough memory ***'
  
  do i = 1, image_size_row
     do j = 1, image_size_col
        image(i,j) = -1
     end do
  end do

  do i = 1, image_size_row
     current_i = -midpoint_x + i 
     do j = 1, image_size_col
        current_j = -midpoint_y + j 
        current_r = scale_factor * sqrt(real(current_i**2 + current_j**2))
        if (current_r < radius) then
              image(i,j) = 0 !max_saturation
           else if ((current_r - scale_factor) < radius) then
              current_x = (current_j - 1) * scale_factor
              if (current_i > 0) then
                 tf = .true.
              else
                 tf = .false.
              end if
              h = scale_factor / max_saturation
              call f_circle(tf, radius, current_x, current_y)
              sum = current_y
              call f_circle(tf, radius, current_x + scale_factor, current_y)
              sum = sum + current_y
              do sub_i = 1, (max_saturation - 2)
                 if (mod(sub_i, 2) == 0) then
                    call f_circle(tf, radius, current_x + (sub_i/max_saturation) * scale_factor, current_y)
                    sum = sum + 4 * current_y
                 else
                    call f_circle(tf, radius, current_x + (sub_i/max_saturation) * scale_factor, current_y)
                    sum = sum + 2 * current_y
                 end if
              end do
              sum = (h / 3) * sum
              image(i, j) = nint(sum) + (max_saturation / 2)
           else
              image(i,j) = max_saturation / 2
        end if
     end do
  end do

  
  
  !call fill_image(image_size_row, image_size_col, scale_factor, radius, max_saturation, image)

  
  do i = 1, image_size_row
     write (out_unit,*) (image(i, j), j = 1, image_size_col)
     !write (*,*) (image(i, j), j = 1, image_size_col)
  end do

end program flow_visualization

subroutine fill_image(image_size_row, &
     image_size_cols, &
     scale_factor, &
     radius, max_saturation, &
     image)
  implicit none
  real, intent(in)  :: scale_factor, radius, max_saturation
  integer, intent(in) :: image_size_row, image_size_cols
  integer, dimension(:, :), allocatable, intent(out) :: image 
  integer :: i, j, current_distance
  real :: radial_length
  real :: r, theta, x_out, y_out
  
  do i = 1, image_size_row
     radial_length = sqrt(radius**2 - (i/scale_factor)**2)
     do j = 1, image_size_cols
        current_distance = current_distance + j / scale_factor
        if (current_distance < radial_length) then
           image(i,j) = max_saturation
        else
           image(i,j) = 0
        end if
     end do
  end do
end subroutine fill_image

subroutine calculate_area(boundry_x, boundry_y, area)
  implicit none
  real, intent(in)  :: boundry_x, boundry_y  ! input
  real, intent(out) :: area                  ! output

  integer :: i, j
  
  
end subroutine calculate_area

subroutine f_circle(tf, radius, x, y)
  implicit none
  real, intent(in) :: x, radius
  logical, intent(in) :: tf
  real, intent(out) :: y

  if (tf .eqv. .true.) then
     y = sqrt(radius**2 - x**2)
  else
     y = sqrt(radius**2 - x**2)
  end if
end subroutine f_circle
